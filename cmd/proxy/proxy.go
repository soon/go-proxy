package main

import (
	"proxy/cfg"
	"proxy/internal/proxy"
)

func main() {
	config := cfg.GetApplicationConfig()
	proxy.RunProxy(config)
}
