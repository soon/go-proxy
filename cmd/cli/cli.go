package main

import (
	"encoding/hex"
	"github.com/abiosoft/readline"
	"github.com/fatih/color"
	"gopkg.in/abiosoft/ishell.v2"
	"log"
	"net/http"
	"proxy/cfg"
	"proxy/internal/analyzer"
	"proxy/internal/db"
	"proxy/internal/ws"
	"strconv"
	"strings"
)

func addCmd(shell *ishell.Shell, cmd *ishell.Cmd) {
	orignalFunc := cmd.Func
	cmd.Func = func(c *ishell.Context) {
		orignalFunc(c)
		c.Println()
	}
	shell.AddCmd(cmd)
}

type CommonRequestConfig struct {
	limitBody int
	binary    bool
}

var green = color.New(color.FgGreen).SprintFunc()
var red = color.New(color.FgRed).SprintFunc()
var yellow = color.New(color.FgYellow).SprintFunc()
var cyan = color.New(color.FgCyan).SprintFunc()
var boldGreen = color.New(color.FgGreen).Add(color.Bold).SprintFunc()
var boldWhite = color.New(color.FgWhite).Add(color.Bold).SprintFunc()
var boldYellow = color.New(color.FgYellow).Add(color.Bold).SprintFunc()
var blueUnderlined = color.New(color.FgBlue).Add(color.Underline).SprintFunc()

func main() {
	config := cfg.GetApplicationConfig()

	color.NoColor = !config.UseColors

	analyzerObj := analyzer.New(config)

	shell := ishell.NewWithConfig(&readline.Config{
		Prompt: boldGreen("♲⚐> "),
	})

	shell.Println("Proxy Shell\n")

	addCmd(shell, &ishell.Cmd{
		Name:    "stolen-flags",
		Help:    "list stolen flags",
		Aliases: []string{"fs"},
		Func: func(c *ishell.Context) {
			limit, err := parseFirstArgAsInt(c, 10)
			if err != nil {
				c.Println(err)
				return
			}
			flags, err := analyzerObj.GetStolenFlags(limit)
			if err != nil {
				c.Println(err)
				return
			}
			printFlagsPage(PrintFlagsPageCommandConfig{
				flags:    flags,
				context:  c,
				analyzer: &analyzerObj,
			})
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name: "fsr",
		Help: "list stolen flags with requests",
		Func: func(c *ishell.Context) {
			limit, err := parseFirstArgAsInt(c, 10)
			if err != nil {
				c.Println(err)
				return
			}
			flags, err := analyzerObj.GetStolenFlags(limit)
			if err != nil {
				c.Println(err)
				return
			}
			printFlagsPage(PrintFlagsPageCommandConfig{
				flags:               flags,
				context:             c,
				analyzer:            &analyzerObj,
				printDetailFlagInfo: true,
			})
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name:    "created-flags",
		Help:    "list created flags",
		Aliases: []string{"fc"},
		Func: func(c *ishell.Context) {
			limit, err := parseFirstArgAsInt(c, 10)
			if err != nil {
				c.Println(err)
				return
			}
			flags, err := analyzerObj.GetCreatedFlags(limit)
			if err != nil {
				c.Println(err)
				return
			}
			printFlagsPage(PrintFlagsPageCommandConfig{
				flags:    flags,
				context:  c,
				analyzer: &analyzerObj,
			})
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name:    "flag",
		Help:    "get flag info",
		Aliases: []string{"f"},
		Func: func(c *ishell.Context) {
			flag := strings.TrimSpace(getSingleArgument(c, "Enter flag: "))
			printFlagInfo(c, &analyzerObj, flag)
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name:    "request",
		Help:    "get request info",
		Aliases: []string{"r", "req"},
		Func: func(c *ishell.Context) {
			requestId := strings.TrimSpace(getSingleArgument(c, "Enter request id: "))
			requestId = cleanRequestId(requestId)
			request, err := analyzerObj.GetRequestInfo(requestId)
			if err != nil {
				c.Println(err)
				return
			}
			config := parseCommonRequestConfig(c.Args)
			delta := request.ResponseTime.Sub(request.RequestTime)
			c.Println(boldYellow(request.Method) + " " + request.Url + " [" + delta.String() + "]")
			if request.IsInterrupted {
				c.Println(red("INTERRUPTED"))
			}
			printHeaders(c, request.RequestHeaders)
			c.Println("Client address: " + request.ClientAddress)
			printRequestData(c, *request, config, "")
			c.Println("\nResponse: " + strconv.Itoa(request.StatusCode))
			printHeaders(c, request.ResponseHeaders)
			printResponseData(c, *request, config, "")
			printWsMessages(c, *request)
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name:    "requests-stats",
		Help:    "get requests stats",
		Aliases: []string{"rs", "rstat"},
		Func: func(c *ishell.Context) {
			requestsCount, err := strconv.Atoi(getSingleArgument(c, "Enter requests count: "))
			if err != nil {
				c.Println(err)
				return
			}
			stats, err := analyzerObj.GetRequestsStats(requestsCount)
			if err != nil {
				c.Println(err)
				return
			}
			for _, entry := range stats {
				c.Printf("%s: %d times\n", entry.Value, entry.Count)
			}
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name:    "requests-chain",
		Help:    "get requests chain",
		Aliases: []string{"rc", "rchain"},
		Func: func(c *ishell.Context) {
			requestId := cleanRequestId(getSingleArgument(c, "Enter request id: "))
			startIdx := 1
			if len(c.Args) == 0 {
				startIdx = 0
			}
			config := parseCommonRequestConfig(c.Args[startIdx:])
			analyzerConfig := parseRequestChainConfig(c.Args[startIdx:])
			chain, err := analyzerObj.GetRequestsChain(requestId, analyzerConfig)
			if err != nil {
				c.Println(err)
				return
			}
			if len(chain) == 0 {
				c.Println("Chain is empty")
				return
			}
			var prevEntry db.RequestLogEntry
			for idx, chainEntry := range chain {
				entry := chainEntry.Request
				if idx == 0 {
					c.Println(
						boldYellow(entry.Method), blueUnderlined(entry.Url), entry.StatusCode, boldWhite(entry.Id),
					)
				} else {
					delta := analyzer.GetRequestsTimeDelta(&entry, &prevEntry)
					c.Println(
						boldYellow(entry.Method), blueUnderlined(entry.Url), entry.StatusCode,
						"[Δresp->req: "+delta.RespReqDelta.String()+"]",
						"[Δreq: "+delta.ReqDelta.String()+"]",
						"[Δresp: "+delta.RespDelta.String()+"]",
						boldWhite(entry.Id),
					)
				}
				if entry.IsInterrupted {
					c.Println(red("INTERRUPTED"))
				}
				printSomeHeaders(c, entry.RequestHeaders, map[string][]string{"Cookie": {}})
				printRequestData(c, entry, config, "> ")
				printSomeHeaders(c, entry.ResponseHeaders, map[string][]string{"Set-Cookie": {}})
				printResponseData(c, entry, config, "< ")
				printWsMessages(c, entry)
				if chainEntry.Skipped > 0 {
					c.Println()
					c.Print(yellow("Skipped ", chainEntry.Skipped, " similar"))
					if chainEntry.Skipped > 1 {
						c.Println(yellow(" entries"))
					} else {
						c.Println(yellow(" entry"))
					}
				}
				if idx < len(chain)-1 {
					c.Println()
				}
				prevEntry = entry
			}
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name:    "user-agents",
		Help:    "Prints user agent stats for the latest N requests",
		Aliases: []string{"ua"},
		Func: func(c *ishell.Context) {
			limit, err := parseFirstArgAsInt(c, 10)
			stats, err := analyzerObj.GetUserAgentStats(limit)
			if err != nil {
				c.Println(red(err))
				return
			}
			printStringsFrequency(c, stats)
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name:    "headers-count",
		Help:    "Prints headers count stats for the latest N requests",
		Aliases: []string{"hc"},
		Func: func(c *ishell.Context) {
			limit, err := parseFirstArgAsInt(c, 10)
			stats, err := analyzerObj.GetHeadersStats(limit)
			if err != nil {
				c.Println(red(err))
				return
			}
			for _, entry := range stats {
				c.Printf("%d %s (%s)\n", entry.Value, yellow("x"+strconv.Itoa(entry.Count)), entry.Sample)
			}
		},
	})
	addCmd(shell, &ishell.Cmd{
		Name: "clear-db",
		Help: "clears db (cannot be undone)",
		Func: func(c *ishell.Context) {
			err := analyzerObj.ClearDb()
			if err != nil {
				c.Println(red(err))
			} else {
				c.Println("Done")
			}
		},
	})
	shell.Run()
}

func formatBool(value bool) string {
	fn := green
	if !value {
		fn = red
	}
	return fn(strconv.FormatBool(value))
}

func printFlagInfo(c *ishell.Context, analyzerObj *analyzer.Analyzer, flag string) {
	flagChanged := false
	if !analyzerObj.IsFlag(flag) && analyzerObj.IsFlag(flag+"=") {
		flag = flag + "="
		flagChanged = true
	}
	info, err := analyzerObj.GetFlagInfo(flag)
	if err != nil {
		c.Println(err)
		return
	}
	if flagChanged {
		c.Println("Flag:", yellow(flag))
	}
	c.Println("Exists:", formatBool(info.Exists))
	if !info.Exists && len(info.StolenRequestIds) == 0 {
		return
	}
	c.Println("Stolen in requests:", strings.Join(info.StolenRequestIds, ", "))
	printStringsFrequency(c, info.UrlFrequency)
	if len(info.InterruptedRequestsIds) > 0 {
		c.Println(red("Interrupted requests:"), strings.Join(info.InterruptedRequestsIds, ", "))
		printStringsFrequency(c, info.InterruptedUrlFrequency)
	}
}

func printStringsFrequency(c *ishell.Context, data []analyzer.StringFrequencyEntry) {
	for _, entry := range data {
		printStringFrequencyEntry(c, entry)
	}
}

func printStringFrequencyEntry(c *ishell.Context, entry analyzer.StringFrequencyEntry) {
	c.Println(entry.Value, yellow("x"+strconv.Itoa(entry.Count)))
}

func printRequestData(c *ishell.Context, entry db.RequestLogEntry, config CommonRequestConfig, prefix string) {
	if len(entry.RequestData) == 0 {
		if strings.ToLower(entry.Method) != "get" {
			c.Println("[empty request body]")
		}
		return
	}
	if config.binary {
		c.Println(cyan(hex.Dump(entry.RequestData)))
	} else {
		c.Println(cyan(prefix, string(entry.RequestData)))
	}
}

func printWsMessages(c *ishell.Context, entry db.RequestLogEntry) {
	for _, message := range entry.WsMessages {
		var prefix string
		if message.IsRequest {
			prefix = "> "
		} else {
			prefix = "< "
		}
		c.Println(cyan(prefix, ws.PrettifyWsFrame(message.Data)))
	}
}

func printResponseData(c *ishell.Context, entry db.RequestLogEntry, config CommonRequestConfig, prefix string) {
	if len(entry.ResponseData) == 0 {
		c.Println(green("[empty response body]"))
		return
	}
	if config.limitBody > 0 && config.limitBody < len(entry.ResponseData) {
		if config.binary {
			c.Println(green(hex.Dump(entry.ResponseData[:config.limitBody])))
		} else {
			c.Println(green(prefix, string(entry.ResponseData)[:config.limitBody]))
		}
	} else {
		if config.binary {
			c.Println(green(hex.Dump(entry.ResponseData)))
		} else {
			c.Println(green(prefix, string(entry.ResponseData)))
		}
	}
}

func parseFirstArgAsInt(c *ishell.Context, def int) (int, error) {
	limit := def
	var err error
	if len(c.Args) > 0 {
		limit, err = strconv.Atoi(c.Args[0])
	}
	return limit, err
}

type PrintFlagsPageCommandConfig struct {
	flags               *analyzer.FlagsPage
	context             *ishell.Context
	analyzer            *analyzer.Analyzer
	printDetailFlagInfo bool
}

func printFlagsPage(config PrintFlagsPageCommandConfig) {
	for _, flag := range config.flags.Flags {
		config.context.Println(yellow(flag))
		if config.printDetailFlagInfo {
			printFlagInfo(config.context, config.analyzer, flag)
			config.context.Println()
		}
	}
	config.context.Println("Showing", len(config.flags.Flags), "of", config.flags.Total)
}

func parseCommonRequestConfig(args []string) CommonRequestConfig {
	config := CommonRequestConfig{
		limitBody: 1000,
		binary:    false,
	}
	for _, arg := range args {
		if strings.HasPrefix(arg, "body-limit:") || strings.HasPrefix(arg, "bl:") {
			parts := strings.Split(arg, ":")
			value, err := strconv.Atoi(parts[1])
			if err != nil {
				log.Print(err)
			} else {
				config.limitBody = value
			}
		}
		if arg == "bin" {
			config.binary = true
		}
	}
	return config
}

func parseRequestChainConfig(args []string) analyzer.RequestChainConfig {
	config := analyzer.RequestChainConfig{
		Session:     true,
		Overlapping: false,
		SkipSimilar: false,
		Limit:       20,
	}

	for _, arg := range args {
		if arg == "-session" || arg == "-s" {
			config.Session = false
		}
		if arg == "+overlapping" || arg == "+o" {
			config.Overlapping = true
		}
		if strings.HasPrefix(arg, "limit:") || strings.HasPrefix(arg, "l:") {
			parts := strings.Split(arg, ":")
			value, err := strconv.Atoi(parts[1])
			if err != nil {
				log.Print(err)
			} else {
				config.Limit = value
			}
		}
		if arg == "-similar" {
			config.SkipSimilar = true
		}
	}
	return config
}

func cleanRequestId(requestId string) string {
	return strings.TrimRight(requestId, ",")
}

func printHeaders(c *ishell.Context, headers http.Header) {
	for header, value := range headers {
		if len(value) == 1 {
			c.Println(header + ": " + value[0])
		} else {
			c.Println(header + ": [" + strings.Join(value, ",") + "]")
		}
	}
}

func printSomeHeaders(c *ishell.Context, headers http.Header, allowedNames map[string][]string) {
	for header, value := range headers {
		_, allowed := allowedNames[header]
		if !allowed {
			continue
		}

		if len(value) == 1 {
			c.Println(header + ": " + value[0])
		} else {
			c.Println(header + ": [" + strings.Join(value, ",") + "]")
		}
	}
}

func getSingleArgument(c *ishell.Context, helpText string) string {
	if len(c.Args) > 0 {
		return c.Args[0]
	}
	c.ShowPrompt(false)
	defer c.ShowPrompt(true)
	c.Print(helpText)
	return c.ReadLine()
}
