package main

import (
	"fmt"
	"log"
	"os"
	"proxy/internal/docker"
)

func main() {
	dockerComposeFile := ""
	if len(os.Args) <= 1 {
		if _, err := os.Stat("docker-compose.yml"); err == nil {
			dockerComposeFile = "docker-compose.yml"
		}
		if _, err := os.Stat("docker-compose.yaml"); err == nil {
			dockerComposeFile = "docker-compose.yaml"
		}
		if dockerComposeFile == "" {
			fmt.Printf("Usage: %s path/to/docker-compose.yml\n", os.Args[0])
			os.Exit(1)
		}
	} else {
		dockerComposeFile = os.Args[1]
	}

	err := docker.AddProxyToDockerCompose(dockerComposeFile)
	if err != nil {
		log.Fatal(err)
	}
}
