#!/bin/bash

set -e

DUMP_FOLDER=./dump
MONGO_ARCHIVE_NAME=dump.archive.gz
REDIS_DUMP_FILE=dump.rdb

MONGO_CONTAINER=`docker-compose ps -q proxy-mongo`
REDIS_CONTAINER=`docker-compose ps -q proxy-redis`

mkdir ${DUMP_FOLDER}

docker-compose exec proxy-mongo mongodump --gzip --archive=${MONGO_ARCHIVE_NAME}
docker cp ${MONGO_CONTAINER}:/${MONGO_ARCHIVE_NAME} ./${DUMP_FOLDER}/mongo.${MONGO_ARCHIVE_NAME}
docker cp ${REDIS_CONTAINER}:/data/${REDIS_DUMP_FILE} ./${DUMP_FOLDER}/redis.${REDIS_DUMP_FILE}
tar -czvf ${DUMP_FOLDER}.tar.gz ${DUMP_FOLDER}
rm -r ${DUMP_FOLDER}
