#!/bin/bash

GOOS=linux go build -o bin/linux/docker-util cmd/docker/dockerUtil.go
GOOS=darwin go build -o bin/darwin/docker-util cmd/docker/dockerUtil.go
