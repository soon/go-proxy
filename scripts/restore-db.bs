#!/bin/bash

set -e

DUMP_FILE=$1

if [ "$DUMP_FILE" = "" ]
then
  if [ -f "./dump.tar.gz" ]
  then
    echo "Using dump.tar.gz file"
    DUMP_FILE=dump.tar.gz
  else
    echo "Usage: $0 <backup-file>"
    exit
  fi
fi

DUMP_FOLDER=./dump
MONGO_ARCHIVE_NAME=mongo.dump.archive.gz
REDIS_DUMP_FILE=redis.dump.rdb
REDIS_RDB_PATH=/data/dump.rdb

tar -xvf "$DUMP_FILE"

MONGO_CONTAINER=`docker-compose ps -q proxy-mongo`
REDIS_CONTAINER=`docker-compose ps -q proxy-redis`

docker cp ./${DUMP_FOLDER}/${MONGO_ARCHIVE_NAME} ${MONGO_CONTAINER}:/
docker-compose exec proxy-mongo mongorestore --gzip --archive=${MONGO_ARCHIVE_NAME}

docker-compose stop redis
docker cp ./${DUMP_FOLDER}/${REDIS_DUMP_FILE} ${REDIS_CONTAINER}:${REDIS_RDB_PATH}
docker-compose start redis
