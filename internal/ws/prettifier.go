package ws

import (
	"encoding/binary"
	"errors"
)

const MaskMask = 0b1000_0000
const LenMask = 0xFF ^ MaskMask

func PrettifyWsFrame(frame []byte) string {
	if len(frame) == 0 {
		return "[empty]"
	}
	switch op := frame[0] & 0xF; {
	case op == 0x0:
		return "continuation"
	case op == 0x1:
		return prettifyTextFrame(frame)
	case op == 0x2:
		return "bin"
	case op >= 0x3 && op <= 0x7:
		return "reserved"
	case op == 0x8:
		return "close"
	case op == 0x9:
		return "ping"
	case op == 0xA:
		return "pong"
	default:
		return "reserved"
	}
}

func readUint16(frame []byte) (uint16, error) {
	if len(frame) < 2 {
		return 0, errors.New("must be at least 2 bytes long")
	}
	return binary.BigEndian.Uint16(frame), nil
}

func readUint64(frame []byte) (uint64, error) {
	if len(frame) < 8 {
		return 0, errors.New("must be at least 8 bytes long")
	}
	return binary.BigEndian.Uint64(frame), nil
}

func min(a uint64, b int) int {
	if a < uint64(b) {
		return int(a)
	}
	return b
}

func prettifyTextFrame(frame []byte) string {
	hasMask := frame[1]&MaskMask == MaskMask
	length := frame[1] & LenMask

	var finalLength uint64
	var err error
	offset := 2
	if length == 126 {
		finalLength16, err := readUint16(frame[offset:])
		if err != nil {
			return "text (invalid len)"
		}
		finalLength = uint64(finalLength16)
		offset += 2
	} else if length == 127 {
		finalLength, err = readUint64(frame[offset:])
		if err != nil {
			return "text (invalid len)"
		}
		offset += 8
	} else {
		finalLength = uint64(length)
	}

	var mask []byte
	if hasMask {
		if len(frame[offset:]) < 4 {
			return "text (invalid mask)"
		}
		mask = frame[offset : offset+4]
		offset += 4
	} else {
		mask = nil
	}

	payload := frame[offset:]
	payload = payload[:min(finalLength, len(payload))]
	if mask != nil {
		payloadCopy := make([]byte, len(payload))
		copy(payloadCopy, payload)
		for i := range payloadCopy {
			payloadCopy[i] ^= mask[i%4]
		}
		payload = payloadCopy
	}

	if uint64(len(payload)) != finalLength {
		return "text (length mismatch) " + string(payload)
	}
	return string(payload)
}
