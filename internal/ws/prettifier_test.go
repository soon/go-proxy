package ws

import (
	"encoding/hex"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPrettifyWsFrame__Close(t *testing.T) {
	t.Parallel()
	tests := []struct {
		frame    string
		expected string
	}{
		{"880203e8", "close"},
		{"810548656c6c6f", "Hello"},
		{"818537fa213d7f9f4d5158", "Hello"},
		{"817e00837b2273656e6465725f6964223a2238383561313536656432222c2264617461223a224040202d302c3020" +
			"2b312c35382040405c6e2b253742253232666c61672532323a20253232363947355530583533485850555a5138504f56" +
			"5454415153324a4746364c583d2532322c20253232666c61675f69642532323a20312537445c6e227d",
			"{\"sender_id\":\"885a156ed2\",\"data\":\"@@ -0,0 +1,58 @@\\n+%7B%22" +
				"flag%22: %2269G5U0X53HXPUZQ8POVTTAQS2JGF6LX=%22, %22flag_id%22: 1%7D\\n\"}"},
	}
	for _, testCase := range tests {
		localTestCase := testCase
		t.Run(localTestCase.frame, func(t *testing.T) {
			t.Parallel()
			frameBytes, err := hex.DecodeString(testCase.frame)
			assert.NoError(t, err)
			assert.Equal(t, testCase.expected, PrettifyWsFrame(frameBytes))
		})
	}
}
