package telegram

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v7"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"proxy/cfg"
	"proxy/internal/analyzer"
	"time"
)

type Telegram struct {
	api      *tgbotapi.BotAPI
	analyzer analyzer.Analyzer
	redis    *redis.Client
	chatId   int64
	appName  string
}

func New(config cfg.ApplicationConfig) (*Telegram, error) {
	if config.TelegramToken == "" {
		log.Fatal("TelegramToken is required")
	}
	if config.TelegramChatId == 0 {
		log.Fatal("TelegramChatId is required")
	}
	api, err := tgbotapi.NewBotAPI(config.TelegramToken)
	if err != nil {
		return nil, err
	}
	redisClient := redis.NewClient(&redis.Options{
		Addr:     config.RedisUrl,
		Password: "",
		DB:       0,
	})
	return &Telegram{
		api:      api,
		analyzer: analyzer.New(config),
		redis:    redisClient,
		chatId:   config.TelegramChatId,
		appName:  config.AppName,
	}, nil
}

type StatusUpdate struct {
	Dt           time.Time
	Prev         *StatusUpdate `json:"-"`
	StolenFlags  int
	CreatedFlags int
}

func (u *StatusUpdate) StolenFlagsDelta() int {
	if u.Prev == nil {
		return u.StolenFlags
	}
	return u.StolenFlags - u.Prev.StolenFlags
}

func (u *StatusUpdate) CreatedFlagsDelta() int {
	if u.Prev == nil {
		return u.CreatedFlags
	}
	return u.CreatedFlags - u.Prev.CreatedFlags
}

func (t *Telegram) sendMessage(text string) (tgbotapi.Message, error) {
	msg := tgbotapi.NewMessage(t.chatId, text)
	return t.api.Send(msg)
}

const RedisStatusUpdateKey = "tg-status-update"

func (t *Telegram) getStatusUpdate() (*StatusUpdate, error) {
	createdFlags, err := t.analyzer.GetCreatedFlags(0)
	if err != nil {
		return nil, err
	}
	stolenFlags, err := t.analyzer.GetStolenFlags(0)
	if err != nil {
		return nil, err
	}
	prevUpdate, err := t.getPrevUpdate(err)
	return &StatusUpdate{
		Dt:           time.Now(),
		Prev:         prevUpdate,
		StolenFlags:  int(stolenFlags.Total),
		CreatedFlags: int(createdFlags.Total),
	}, nil
}

func (t *Telegram) saveStatusUpdate(status *StatusUpdate) error {
	statusJson, err := json.Marshal(status)
	if err != nil {
		return err
	}
	_, err = t.redis.Set(RedisStatusUpdateKey, string(statusJson), 0).Result()
	return err
}

func (t *Telegram) getPrevUpdate(err error) (*StatusUpdate, error) {
	prevUpdateStr, err := t.redis.Get(RedisStatusUpdateKey).Result()
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	var prevUpdate StatusUpdate
	err = json.Unmarshal([]byte(prevUpdateStr), &prevUpdate)
	if err != nil {
		return nil, err
	}
	return &prevUpdate, nil
}

func (t *Telegram) SendUpdate() error {
	update, err := t.getStatusUpdate()
	if err != nil {
		return err
	}
	text := fmt.Sprintf("%v: \n⚑ Created %v (%+d), \n⚑ Stolen %v (%+d)",
		t.appName, update.CreatedFlags,
		update.CreatedFlagsDelta(), update.StolenFlags, update.StolenFlagsDelta(),
	)
	_, err = t.sendMessage(text)
	if err != nil {
		return err
	}
	return t.saveStatusUpdate(update)
}

func (t *Telegram) SendImAliveMessage() error {
	_, err := t.sendMessage(fmt.Sprintf("%v: I'm alive!", t.appName))
	return err
}
