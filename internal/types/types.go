package types

import (
	"github.com/google/uuid"
	"net/http"
	"time"
)

type RequestLogEntry struct {
	Id              string
	App             string
	RequestHeaders  http.Header
	RequestData     []byte
	RequestTime     time.Time
	ResponseHeaders http.Header
	ResponseData    []byte
	ResponseTime    time.Time
	StatusCode      int
	Url             string
	Method          string
	ClientAddress   string
	IsWs            bool
	IsInterrupted   bool
}

func GenerateRandomId() string {
	return uuid.New().String()
}

func GenerateRequestId() string {
	return GenerateRandomId()
}

type WebSocketMessage struct {
	Id        string
	LogEntry  RequestLogEntry
	Data      []byte
	IsRequest bool
	IsLast    bool
}

func GenerateWebSocketMessageId() string {
	return GenerateRandomId()
}

type RequestInterruptedError struct {
}

func NewRequestInterruptedError() error {
	return &RequestInterruptedError{}
}

func (e *RequestInterruptedError) Error() string {
	return "request interrupted"
}
