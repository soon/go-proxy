package analyzer

import (
	"github.com/go-redis/redis/v7"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/url"
	"proxy/cfg"
	"proxy/internal/db"
	"proxy/internal/types"
	"proxy/internal/ws"
	"regexp"
	"sort"
	"strings"
	"time"
)

type Analyzer struct {
	flagRe       *regexp.Regexp
	adminsRe     *regexp.Regexp
	redis        *redis.Client
	dbConnection *db.Connection
}

type UrlFrequencyEntry struct {
	Url       string
	Frequency int
}

type StringFrequencyEntry struct {
	Value string
	Count int
}

type IntFrequencyEntry struct {
	Value  int
	Sample string
	Count  int
}

type FlagInfo struct {
	Exists                  bool
	StolenRequestIds        []string
	UrlFrequency            []StringFrequencyEntry
	InterruptedRequestsIds  []string
	InterruptedUrlFrequency []StringFrequencyEntry
}

func New(config cfg.ApplicationConfig) Analyzer {
	flagRe, err := regexp.Compile(config.FlagRegex)
	if err != nil {
		log.Fatal(err)
	}
	adminsRe, err := regexp.Compile(config.AdminIpRegex)
	if err != nil {
		log.Fatal(err)
	}
	client := redis.NewClient(&redis.Options{
		Addr:     config.RedisUrl,
		Password: "",
		DB:       0,
	})
	dbConnection, err := db.New(config.MongoUrl, config.AppName)
	if err != nil {
		log.Fatal(err)
	}
	return Analyzer{
		flagRe:       flagRe,
		adminsRe:     adminsRe,
		redis:        client,
		dbConnection: dbConnection,
	}
}

func (a *Analyzer) AnalyzeRequest(entry types.RequestLogEntry) {
	requestFlags := a.getRequestFlags(entry)
	requestFlagsMap := make(map[string]struct{})
	for _, flag := range requestFlags {
		a.logFlagCreation(flag, entry)
		requestFlagsMap[flag] = struct{}{}
	}
	responseFlags := a.getResponseFlags(entry)
	for _, flag := range responseFlags {
		if _, flagIsInRequest := requestFlagsMap[flag]; !flagIsInRequest {
			a.logFlagStealing(flag, entry)
		}
	}
}

func (a *Analyzer) AnalyzeWsRequest(entry types.WebSocketMessage) {
	flags := a.findAllFlags(entry.Data, true)
	requestFlagsMap := make(map[string]struct{})
	for _, flag := range flags {
		if entry.IsRequest {
			a.logFlagCreation(flag, entry.LogEntry)
			requestFlagsMap[flag] = struct{}{}
		} else {
			if _, flagIsInRequest := requestFlagsMap[flag]; !flagIsInRequest {
				a.logFlagStealing(flag, entry.LogEntry)
			}
		}
	}
}

func (a *Analyzer) getRequestFlags(entry types.RequestLogEntry) []string {
	return a.findAllFlags(entry.RequestData, false)
}

func (a *Analyzer) getResponseFlags(entry types.RequestLogEntry) []string {
	return a.findAllFlags(entry.ResponseData, false)
}

func unique(slice []string) []string {
	uniqMap := make(map[string]struct{})
	for _, v := range slice {
		uniqMap[v] = struct{}{}
	}

	uniqSlice := make([]string, 0, len(uniqMap))
	for v := range uniqMap {
		uniqSlice = append(uniqSlice, v)
	}
	return uniqSlice
}

func (a *Analyzer) findAllFlags(data []byte, isWs bool) []string {
	if data == nil {
		return nil
	}
	dataStr := string(data)
	flags := a.flagRe.FindAllString(dataStr, -1)
	unescapedData, err := url.QueryUnescape(dataStr)
	if err == nil {
		otherFlags := a.flagRe.FindAllString(unescapedData, -1)
		flags = append(flags, otherFlags...)
	}
	if isWs {
		decodedData := ws.PrettifyWsFrame(data)
		otherFlags := a.flagRe.FindAllString(decodedData, -1)
		flags = append(flags, otherFlags...)
	}
	return unique(flags)
}

func (a *Analyzer) IsFlag(flag string) bool {
	return a.flagRe.MatchString(flag)
}

const RedisKeyPrefix = "proxy-flags:"
const CreatedFlagsKey = RedisKeyPrefix + "created"
const StolenFlagsKey = RedisKeyPrefix + "stolen"

func (a *Analyzer) isFlagKnown(flag string) bool {
	key := RedisKeyPrefix + flag
	_, err := a.redis.Get(key).Result()
	if err != nil && err != redis.Nil {
		log.Print(err)
	}
	return err != redis.Nil
}

func (a *Analyzer) logFlagCreation(flag string, entry types.RequestLogEntry) {
	totalFlags, err := a.redis.ZCard(CreatedFlagsKey).Result()
	if err != nil {
		log.Print(err)
		return
	}
	_, err = a.redis.ZAddNX(CreatedFlagsKey, &redis.Z{
		Score:  float64(totalFlags + 1),
		Member: flag,
	}).Result()
	if err != nil {
		log.Print(err)
	}
	key := RedisKeyPrefix + flag + ":creation"
	_, err = a.redis.LPush(key, entry.Id).Result()
	if err != nil {
		log.Print(err)
	}
}

func getFlagStealingKey(flag string) string {
	return RedisKeyPrefix + flag + ":stealing"
}

func getFlagInterruptedStealingKey(flag string) string {
	return RedisKeyPrefix + flag + ":interrupted-stealing"
}

func (a *Analyzer) logFlagStealing(flag string, entry types.RequestLogEntry) {
	if !entry.IsInterrupted {
		totalFlags, err := a.redis.ZCard(StolenFlagsKey).Result()
		if err != nil {
			log.Print(err)
			return
		}
		flagStr := flag
		_, err = a.redis.ZAddNX(StolenFlagsKey, &redis.Z{
			Score:  float64(totalFlags + 1),
			Member: flagStr,
		}).Result()
		if err != nil {
			log.Print(err)
		}
	}
	key := getFlagStealingKey(flag)
	if entry.IsInterrupted {
		key = getFlagInterruptedStealingKey(flag)
	}
	_, err := a.redis.LPush(key, entry.Id).Result()
	if err != nil {
		log.Print(err)
	}
}

type FlagsPage struct {
	Flags []string
	Total int64
}

func (a *Analyzer) GetCreatedFlags(limit int) (*FlagsPage, error) {
	return a.getFlagsPageFromOrderedSet(CreatedFlagsKey, limit)
}

func (a *Analyzer) GetStolenFlags(limit int) (*FlagsPage, error) {
	return a.getFlagsPageFromOrderedSet(StolenFlagsKey, limit)
}

func (a *Analyzer) getFlagsPageFromOrderedSet(key string, limit int) (*FlagsPage, error) {
	totalFlags, err := a.redis.ZCard(key).Result()
	if err != nil {
		return nil, err
	}
	if limit == 0 {
		return &FlagsPage{
			Flags: nil,
			Total: totalFlags,
		}, nil
	}
	if limit > 0 {
		limit -= 1
	} else {
		limit = -1
	}
	flags, err := a.redis.ZRevRange(key, 0, int64(limit)).Result()
	if err != nil {
		return nil, err
	}
	return &FlagsPage{
		Flags: flags,
		Total: totalFlags,
	}, nil
}

func (a *Analyzer) GetFlagInfo(flag string) (*FlagInfo, error) {
	res := FlagInfo{}
	creationSuffix := ":creation"
	exists, err := a.redis.Exists(RedisKeyPrefix + flag + creationSuffix).Result()
	if err != nil {
		return nil, err
	}
	res.Exists = exists > 0
	requestIds, err := a.redis.LRange(getFlagStealingKey(flag), 0, -1).Result()
	if err == nil {
		res.StolenRequestIds = requestIds
	} else if err != redis.Nil {
		return nil, err
	}
	logs, err := a.dbConnection.FindLogEntriesByIds(res.StolenRequestIds)
	if err != nil {
		return nil, err
	}
	res.UrlFrequency = countLogsUrlFrequency(logs)
	interruptedRequestsIds, err := a.redis.LRange(getFlagInterruptedStealingKey(flag), 0, -1).Result()
	if err == nil {
		res.InterruptedRequestsIds = interruptedRequestsIds
	} else if err != redis.Nil {
		return nil, err
	}
	interruptedLogs, err := a.dbConnection.FindLogEntriesByIds(res.InterruptedRequestsIds)
	if err != nil {
		return nil, err
	}
	res.InterruptedUrlFrequency = countLogsUrlFrequency(interruptedLogs)
	return &res, nil
}

func countLogsUrlFrequency(logs []db.RequestLogEntry) []StringFrequencyEntry {
	return countLogsFrequency(logs, func(log db.RequestLogEntry) string {
		return log.Url
	})
}

func countLogsFrequency(logs []db.RequestLogEntry, getter func(log db.RequestLogEntry) string) []StringFrequencyEntry {
	valueToCount := make(map[string]int)
	for _, logEntry := range logs {
		valueToCount[getter(logEntry)] += 1
	}
	frequency := make([]StringFrequencyEntry, 0, len(valueToCount))
	for value, count := range valueToCount {
		frequency = append(frequency, StringFrequencyEntry{
			Value: value,
			Count: count,
		})
	}
	sort.Slice(frequency, func(i, j int) bool {
		return frequency[i].Count > frequency[j].Count
	})
	return frequency
}

func (a *Analyzer) GetRequestInfo(requestId string) (*db.RequestLogEntry, error) {
	return a.dbConnection.FindLogEntryById(requestId)
}

func (a *Analyzer) GetRequestsStats(count int) ([]StringFrequencyEntry, error) {
	logs, err := a.dbConnection.FindLogEntries(bson.M{}, options.Find().SetLimit(int64(count)))
	if err != nil {
		return nil, err
	}
	return countLogsUrlFrequency(logs), nil
}

func (a *Analyzer) GetUserAgentStats(count int) ([]StringFrequencyEntry, error) {
	logs, err := a.dbConnection.FindLogEntries(bson.M{}, options.Find().SetLimit(int64(count)))
	if err != nil {
		return nil, err
	}
	return countLogsFrequency(logs, func(log db.RequestLogEntry) string {
		return log.RequestHeaders.Get("User-Agent")
	}), nil
}

func (a *Analyzer) GetHeadersStats(count int) ([]IntFrequencyEntry, error) {
	logs, err := a.dbConnection.FindLogEntries(bson.M{}, options.Find().SetLimit(int64(count)))
	if err != nil {
		return nil, err
	}
	valueToCount := make(map[int]int)
	valueToReqId := make(map[int]string)
	for _, logEntry := range logs {
		valueToCount[len(logEntry.RequestHeaders)] += 1
		valueToReqId[len(logEntry.RequestHeaders)] = logEntry.Id
	}
	frequency := make([]IntFrequencyEntry, 0, len(valueToCount))
	for value, count := range valueToCount {
		frequency = append(frequency, IntFrequencyEntry{
			Value:  value,
			Sample: valueToReqId[value],
			Count:  count,
		})
	}
	sort.Slice(frequency, func(i, j int) bool {
		return frequency[i].Count > frequency[j].Count
	})
	return frequency, nil
}

type RequestChainConfig struct {
	Session     bool
	Overlapping bool
	SkipSimilar bool
	Limit       int
}

type RequestChainEntry struct {
	Request db.RequestLogEntry
	Skipped int
}

func (a *Analyzer) GetRequestsChain(requestId string, cfg RequestChainConfig) ([]RequestChainEntry, error) {
	res := make([]RequestChainEntry, 0)
	finalRequest, err := a.GetRequestInfo(requestId)
	if err != nil {
		return nil, err
	}
	targetSession := getRequestSessionCookieValue(finalRequest)
	var prevReq *db.RequestLogEntry

	selector := func(req *db.RequestLogEntry) bool {
		cond := filterBySession(cfg, targetSession, req) && filterByOverlapping(cfg, prevReq, req)
		if cond {
			prevReq = req
			if len(res) > 0 && shouldBeSkipped(cfg, req, res[len(res)-1].Request) {
				res[len(res)-1].Skipped += 1
			} else {
				res = append(res, RequestChainEntry{
					Request: *req,
					Skipped: 0,
				})
			}
		}
		return cfg.Limit <= 0 || len(res) < cfg.Limit
	}
	err = a.dbConnection.IterateLogEntries(
		selector,
		bson.M{
			"clientaddress": finalRequest.ClientAddress,
			"requesttime":   bson.M{"$lte": finalRequest.RequestTime},
		},
		options.Find().SetSort(bson.D{
			{"requesttime", -1},
		}),
	)
	if err != nil {
		return nil, err
	}
	return res, err
}

func shouldBeSkipped(config RequestChainConfig, newReq *db.RequestLogEntry, prevReq db.RequestLogEntry) bool {
	if !config.SkipSimilar {
		return false
	}
	return newReq.Url == prevReq.Url && newReq.Method == prevReq.Method
}

func filterBySession(cfg RequestChainConfig, targetSession string, e *db.RequestLogEntry) bool {
	if !cfg.Session {
		return true
	}
	if targetSession == "" {
		return true
	}
	session := getRequestSessionCookieValue(e)
	if session == targetSession {
		return true
	}
	responseSession := getResponseSetCookieSessionValue(e)
	return responseSession == targetSession
}

func filterByOverlapping(cfg RequestChainConfig, prevReq *db.RequestLogEntry, currReq *db.RequestLogEntry) bool {
	if cfg.Overlapping || prevReq == nil {
		return true
	}
	delta := GetRequestsTimeDelta(currReq, prevReq)
	return delta.RespReqDelta > 0
}

func getRequestSessionCookieValue(log *db.RequestLogEntry) string {
	cookies := strings.Join(log.RequestHeaders["Cookie"], ";")
	values := strings.Split(cookies, ";")
	for _, pair := range values {
		nameAndValue := strings.Split(pair, "=")
		if len(nameAndValue) == 2 && nameAndValue[0] == "session" {
			return nameAndValue[1]
		}
	}
	return ""
}

func getResponseSetCookieSessionValue(log *db.RequestLogEntry) string {
	cookies := log.ResponseHeaders["Set-Cookie"]
	for _, cookie := range cookies {
		parts := strings.Split(cookie, ";")
		nameAndValue := strings.Split(parts[0], "=")
		if len(nameAndValue) == 2 && nameAndValue[0] == "session" {
			return nameAndValue[1]
		}
	}
	return ""
}

type RequestsTimeDelta struct {
	RespReqDelta time.Duration
	ReqDelta     time.Duration
	RespDelta    time.Duration
}

func GetRequestsTimeDelta(first *db.RequestLogEntry, last *db.RequestLogEntry) RequestsTimeDelta {
	return RequestsTimeDelta{
		RespReqDelta: last.RequestTime.Sub(first.ResponseTime),
		ReqDelta:     last.RequestTime.Sub(first.RequestTime),
		RespDelta:    last.ResponseTime.Sub(first.ResponseTime),
	}
}

func (a *Analyzer) ClearDb() error {
	redisKeys, err := a.redis.Keys(RedisKeyPrefix + "*").Result()
	if err != nil {
		return err
	}
	if len(redisKeys) > 0 {
		_, err = a.redis.Del(redisKeys...).Result()
		if err != nil {
			return err
		}
	}
	return a.dbConnection.DeleteRequestsDocuments()
}
