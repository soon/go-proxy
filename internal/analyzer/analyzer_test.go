package analyzer

import (
	"github.com/stretchr/testify/assert"
	"regexp"
	"testing"
)

func TestFindAllFlags(t *testing.T) {
	flagRe, err := regexp.Compile("[A-Z0-9]{31}=")
	if err != nil {
		t.Error(err)
	}
	a := &Analyzer{
		flagRe:       flagRe,
		adminsRe:     nil,
		redis:        nil,
		dbConnection: nil,
	}
	flags := a.findAllFlags([]byte("flag=9G5U0X53HXPUZQ8POVTTAQS2JGF6LX6%3D"), false)
	assert.Equal(t, []string{"9G5U0X53HXPUZQ8POVTTAQS2JGF6LX6="}, flags)
}
