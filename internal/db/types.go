package db

import (
	"net/http"
	"proxy/internal/types"
	"time"
)

type RequestLogEntry struct {
	Id              string
	App             string
	RequestHeaders  http.Header
	RequestData     []byte
	RequestTime     time.Time
	ResponseHeaders http.Header
	ResponseData    []byte
	ResponseTime    time.Time
	StatusCode      int
	Url             string
	Method          string
	ClientAddress   string
	IsWs            bool
	IsWsCompleted   bool
	WsMessages      []WebSocketMessage
	IsInterrupted   bool
}

func FromProxyRequest(r types.RequestLogEntry) RequestLogEntry {
	return RequestLogEntry{
		Id:              r.Id,
		App:             r.App,
		RequestHeaders:  r.RequestHeaders,
		RequestData:     r.RequestData,
		RequestTime:     r.RequestTime,
		ResponseHeaders: r.ResponseHeaders,
		ResponseData:    r.ResponseData,
		ResponseTime:    r.ResponseTime,
		StatusCode:      r.StatusCode,
		Url:             r.Url,
		Method:          r.Method,
		ClientAddress:   r.ClientAddress,
		IsWs:            r.IsWs,
		IsInterrupted:   r.IsInterrupted,
		IsWsCompleted:   false,
		WsMessages:      nil,
	}
}

type WebSocketMessage struct {
	Id        string
	RequestId string
	Data      []byte
	IsRequest bool
}

func FromProxyWs(ws types.WebSocketMessage) WebSocketMessage {
	return WebSocketMessage{
		Id:        ws.Id,
		RequestId: ws.LogEntry.Id,
		Data:      ws.Data,
		IsRequest: ws.IsRequest,
	}
}
