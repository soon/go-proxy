package db

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"proxy/internal/types"
)

type Connection struct {
	mongo   *mongo.Client
	appName string
}

func New(mongoUri string, appName string) (*Connection, error) {
	clientOptions := options.Client().ApplyURI(mongoUri)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, err
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}
	l := Connection{
		mongo:   client,
		appName: appName,
	}
	return &l, nil
}

func (c *Connection) Setup() {
	c.createLogsIndex(mongo.IndexModel{
		Keys: bson.M{
			"id": "hashed",
		},
	})
	c.createLogsIndex(mongo.IndexModel{
		Keys: bson.M{
			"clientaddress": "hashed",
		},
	})
	c.createLogsIndex(mongo.IndexModel{
		Keys: bson.M{
			"requesttime": 1,
		},
	})
	c.createLogsIndex(mongo.IndexModel{
		Keys: bson.M{
			"responsetime": 1,
		},
	})

	c.createWsIndex(mongo.IndexModel{
		Keys: bson.M{
			"id": "hashed",
		},
	})
	c.createWsIndex(mongo.IndexModel{
		Keys: bson.M{
			"requestid": 1,
		},
	})
}

func (c *Connection) createLogsIndex(index mongo.IndexModel) {
	_, err := c.GetLogsCollection().Indexes().CreateOne(context.TODO(), index)
	if err != nil {
		log.Fatal(err)
	}
}

func (c *Connection) createWsIndex(index mongo.IndexModel) {
	_, err := c.GetWsCollection().Indexes().CreateOne(context.TODO(), index)
	if err != nil {
		log.Fatal(err)
	}
}

func (c *Connection) GetLogsCollection() *mongo.Collection {
	return c.getDB().Collection("logs" + c.appName)
}

func (c *Connection) GetWsCollection() *mongo.Collection {
	return c.getDB().Collection("ws" + c.appName)
}

func (c *Connection) DeleteRequestsDocuments() error {
	_, err := c.GetLogsCollection().DeleteMany(context.TODO(), bson.M{})
	if err != nil {
		return err
	}
	_, err = c.GetWsCollection().DeleteMany(context.TODO(), bson.M{})
	return err
}

func (c *Connection) getDB() *mongo.Database {
	return c.mongo.Database("proxyLogs")
}

func (c *Connection) AddLogEntry(entry types.RequestLogEntry) error {
	_, err := c.GetLogsCollection().InsertOne(context.TODO(), FromProxyRequest(entry))
	return err
}

func (c *Connection) AddWsLogEntry(wsEntry types.WebSocketMessage) error {
	if !(wsEntry.Data == nil && wsEntry.IsLast) {
		_, err := c.GetWsCollection().InsertOne(context.TODO(), FromProxyWs(wsEntry))
		if err != nil {
			return err
		}
	}
	logEntry, err := c.FindLogEntryById(wsEntry.LogEntry.Id)
	if _, ok := err.(*NotFoundError); ok {
		return nil
	}
	if err != nil {
		return err
	}
	messages, err := c.FindWsMessagedByLogEntryId(logEntry.Id)
	if err != nil {
		return err
	}
	logEntry.IsWsCompleted = logEntry.IsWsCompleted || wsEntry.IsLast
	logEntry.WsMessages = messages
	_, err = c.GetLogsCollection().UpdateOne(
		context.TODO(),
		bson.M{"id": logEntry.Id},
		bson.M{"$set": bson.M{
			"iswscompleted": logEntry.IsWsCompleted,
			"wsmessages":    logEntry.WsMessages,
		}},
	)
	return err
}

func (c *Connection) FindLogEntriesByIds(ids []string) ([]RequestLogEntry, error) {
	return c.FindLogEntries(bson.M{"id": bson.M{"$in": ids}})
}

type NotFoundError struct {
}

func (e *NotFoundError) Error() string {
	return "not found"
}

func (c *Connection) FindLogEntryById(id string) (*RequestLogEntry, error) {
	ids := make([]string, 0, 1)
	ids = append(ids, id)
	logs, err := c.FindLogEntriesByIds(ids)
	if err != nil {
		return nil, err
	}
	if len(logs) > 0 {
		return &logs[0], nil
	}
	return nil, &NotFoundError{}
}

func (c *Connection) FindWsMessagedByLogEntryId(id string) ([]WebSocketMessage, error) {
	res := make([]WebSocketMessage, 0)
	err := c.IterateWsMessages(func(message *WebSocketMessage) bool {
		res = append(res, *message)
		return true
	}, bson.M{"requestid": id})
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (c *Connection) FindLogEntries(filter interface{}, opts ...*options.FindOptions) ([]RequestLogEntry, error) {
	return c.TakeLogEntriesWhile(func(entry *RequestLogEntry) bool { return true }, filter, opts...)
}

func (c *Connection) FilterLogEntries(selector func(*RequestLogEntry) bool,
	filter interface{}, opts ...*options.FindOptions) ([]RequestLogEntry, error) {
	res := make([]RequestLogEntry, 0)
	finalSelector := func(entry *RequestLogEntry) bool {
		if selector(entry) {
			res = append(res, *entry)
		}
		return true
	}
	err := c.IterateLogEntries(finalSelector, filter, opts...)
	if err != nil {
		return nil, err
	}
	return res, err
}

func (c *Connection) TakeLogEntriesWhile(selector func(*RequestLogEntry) bool,
	filter interface{}, opts ...*options.FindOptions) ([]RequestLogEntry, error) {
	res := make([]RequestLogEntry, 0)
	finalSelector := func(entry *RequestLogEntry) bool {
		cond := selector(entry)
		if cond {
			res = append(res, *entry)
		}
		return cond
	}
	err := c.IterateLogEntries(finalSelector, filter, opts...)
	if err != nil {
		return nil, err
	}
	return res, err
}

func (c *Connection) IterateLogEntries(
	selector func(*RequestLogEntry) bool,
	filter interface{}, opts ...*options.FindOptions) error {

	cur, err := c.GetLogsCollection().Find(context.TODO(), filter, opts...)
	if err != nil {
		return err
	}
	for cur.Next(context.TODO()) {
		var result RequestLogEntry
		err := cur.Decode(&result)
		if err != nil {
			log.Println(err)
		}
		if !selector(&result) {
			break
		}
	}
	if err := cur.Err(); err != nil {
		log.Println(err)
	}
	return nil
}

func (c *Connection) IterateWsMessages(selector func(message *WebSocketMessage) bool,
	filter interface{}, opts ...*options.FindOptions) error {

	cur, err := c.GetWsCollection().Find(context.TODO(), filter, opts...)
	if err != nil {
		return err
	}
	for cur.Next(context.TODO()) {
		var result WebSocketMessage
		err := cur.Decode(&result)
		if err != nil {
			log.Println(err)
		}
		if !selector(&result) {
			break
		}
	}
	if err := cur.Err(); err != nil {
		log.Println(err)
	}
	return nil
}
