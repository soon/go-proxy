package proxy

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"proxy/cfg"
	"proxy/internal/analyzer"
	"proxy/internal/telegram"
	"proxy/internal/types"
	proxyPlugin "proxy/plugin/proxy"
	"strings"
	"time"
)
import _ "net/http/pprof"

const (
	XRequestedAtHeader = "X-Requested-At"
)

func logEntryHandler(logs <-chan types.RequestLogEntry, logger *Logger, analyzer analyzer.Analyzer) {
	for logEntry := range logs {
		analyzer.AnalyzeRequest(logEntry)
		err := logger.LogEntry(logEntry)
		if err != nil {
			log.Print(err)
		}
	}
}

func convertBodyToBytes(body io.ReadCloser) ([]byte, error) {
	if body == nil {
		return nil, nil
	}
	if hijackedBody, ok := body.(*HijackedBody); ok {
		return hijackedBody.Data.Bytes(), nil
	}
	data, err := ioutil.ReadAll(body)
	if err != nil {
		return nil, err
	}
	_ = body.Close()
	return data, nil
}

type HijackedWebSocket struct {
	Body     io.ReadWriteCloser
	Messages []types.WebSocketMessage
	LogEntry types.RequestLogEntry
	Logs     chan types.WebSocketMessage
	Id       string
}

func webSocketLogEntryHandler(logs <-chan types.WebSocketMessage, logger *Logger, analyzer analyzer.Analyzer) {
	for logEntry := range logs {
		analyzer.AnalyzeWsRequest(logEntry)
		err := logger.LogWsEntry(logEntry)
		if err != nil {
			log.Print(err)
		}
	}
}

func (s *HijackedWebSocket) addWsMessage(body []byte, isRequest bool) {
	message := types.WebSocketMessage{
		Id:        types.GenerateWebSocketMessageId(),
		LogEntry:  s.LogEntry,
		Data:      body,
		IsRequest: isRequest,
	}
	s.Messages = append(s.Messages, message)
	s.Logs <- message
}

func (s *HijackedWebSocket) Read(p []byte) (int, error) {
	n, err := s.Body.Read(p)
	if n == 0 {
		return n, err
	}
	dataCopy := make([]byte, n)
	copy(dataCopy, p)
	s.addWsMessage(dataCopy, false)
	return n, err
}

func (s *HijackedWebSocket) Write(p []byte) (int, error) {
	n, err := s.Body.Write(p)
	if n == 0 {
		return n, err
	}
	dataCopy := make([]byte, n)
	copy(dataCopy, p)
	s.addWsMessage(dataCopy, true)
	return n, err
}

func (s *HijackedWebSocket) Close() error {
	err := s.Body.Close()
	s.Logs <- types.WebSocketMessage{
		LogEntry: s.LogEntry,
		IsLast:   true,
	}
	return err
}

func createHijackedWebSocket(
	logEntry types.RequestLogEntry, body io.ReadWriteCloser, logs chan types.WebSocketMessage) HijackedWebSocket {

	return HijackedWebSocket{
		Body:     body,
		Messages: make([]types.WebSocketMessage, 0),
		LogEntry: logEntry,
		Id:       "123",
		Logs:     logs,
	}
}

func shouldLogResponseBody(resp http.Response, config cfg.ApplicationConfig) bool {
	return true
	if config.LogResponseBodyWhenContentTypeIs == nil {
		return false
	}
	if len(config.LogResponseBodyWhenContentTypeIs) == 1 && config.LogResponseBodyWhenContentTypeIs[0] == "any" {
		return true
	}

	if resp.Header == nil {
		return false
	}
	contentType := resp.Header["Content-Type"]
	if contentType == nil {
		return false
	}

	for _, v := range contentType {
		for _, targetValue := range config.LogResponseBodyWhenContentTypeIs {
			if strings.Contains(v, targetValue) {
				return true
			}
		}
	}

	return false
}

func extractIpFromAddr(s string) string {
	host, _, err := net.SplitHostPort(s)
	if err != nil {
		return s
	}
	return host
}

func getClientIp(resp *http.Response, config cfg.ApplicationConfig) string {
	if config.UseIpFromXForwardedFor {
		value := strings.TrimSpace(resp.Request.Header.Get("X-Forwarded-For"))
		if value != "" {
			firstIp := strings.Split(value, ",")[0]
			return extractIpFromAddr(strings.TrimSpace(firstIp))
		}
	}
	return extractIpFromAddr(resp.Request.RemoteAddr)
}

func createProxy(
	target *url.URL, logs chan types.RequestLogEntry,
	wsLogs chan types.WebSocketMessage, config cfg.ApplicationConfig) *httputil.ReverseProxy {

	director := func(req *http.Request) {
		req.URL.Scheme = target.Scheme
		req.URL.Host = target.Host
		req.Header.Del(XRequestedAtHeader)
		req.Header.Add(XRequestedAtHeader, time.Now().Format(time.RFC3339Nano))
		if req.Body != nil {
			req.Body = NewHijackedBody(req.Body)
		}
	}
	modifyResponse := func(resp *http.Response) error {
		responseTime := time.Now()
		reqBody, err := convertBodyToBytes(resp.Request.Body)
		if err != nil {
			log.Print("Unable to get request body", err)
			return nil
		}
		if reqBody != nil {
			bodyCopy := make([]byte, len(reqBody))
			copy(bodyCopy, reqBody)
			resp.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyCopy))
		}

		respBody := ([]byte)(nil)

		if resp.StatusCode != http.StatusSwitchingProtocols && shouldLogResponseBody(*resp, config) {
			respBody, err = convertBodyToBytes(resp.Body)
			if err != nil {
				log.Print("Unable to get response body", err)
				return nil
			}
		}
		if respBody != nil {
			bodyCopy := make([]byte, len(respBody))
			copy(bodyCopy, respBody)
			resp.Body = ioutil.NopCloser(bytes.NewBuffer(bodyCopy))
		}

		isWs := resp.StatusCode == http.StatusSwitchingProtocols
		payload := types.RequestLogEntry{
			Id:              types.GenerateRequestId(),
			App:             config.AppName,
			RequestData:     reqBody,
			RequestHeaders:  resp.Request.Header,
			RequestTime:     getRequestTime(resp),
			ResponseHeaders: resp.Header,
			ResponseData:    respBody,
			ResponseTime:    responseTime,
			StatusCode:      resp.StatusCode,
			Url:             resp.Request.URL.String(),
			Method:          resp.Request.Method,
			ClientAddress:   getClientIp(resp, config),
			IsWs:            isWs,
		}
		if proxyPlugin.ShouldInterruptRequest(payload) {
			payload.IsInterrupted = true
		}
		logs <- payload
		if payload.IsInterrupted {
			return types.NewRequestInterruptedError()
		}

		if isWs {
			if backConn, ok := resp.Body.(io.ReadWriteCloser); ok {
				hijacked := createHijackedWebSocket(payload, backConn, wsLogs)
				resp.Body = &hijacked
			}
		}

		return nil
	}
	errorHandler := func(rw http.ResponseWriter, req *http.Request, err error) {
		if _, ok := err.(*types.RequestInterruptedError); ok {
			rw.WriteHeader(http.StatusOK)
			return
		}
		log.Printf("http: proxy error: %v", err)
		rw.WriteHeader(http.StatusBadGateway)
	}
	return &httputil.ReverseProxy{
		Director:       director,
		ModifyResponse: modifyResponse,
		ErrorHandler:   errorHandler,
		FlushInterval:  -1,
	}
}

func getRequestTime(resp *http.Response) time.Time {
	requestedAt := resp.Request.Header.Get(XRequestedAtHeader)
	if requestedAt != "" {
		value, err := time.Parse(time.RFC3339Nano, requestedAt)
		if err == nil {
			return value
		}
	}
	return time.Now()
}

func runTelegramLoop(config cfg.ApplicationConfig) error {
	if config.TelegramToken == "" {
		return errors.New("telegram token is missing")
	}
	if config.TelegramChatId == 0 {
		return errors.New("telegram chat id is missing")
	}
	tg, err := telegram.New(config)
	if err != nil {
		return err
	}
	err = tg.SendImAliveMessage()
	if err != nil {
		return err
	}
	ticker := time.NewTicker(60 * time.Second)
	for {
		select {
		case _ = <-ticker.C:
			err := tg.SendUpdate()
			if err != nil {
				log.Println(err)
			}
		}
	}
}

func RunProxy(config cfg.ApplicationConfig) {
	if config.EnablePprof {
		go func() {
			log.Println(http.ListenAndServe(":6060", nil))
		}()
	}
	if config.TelegramToken != "" || config.TelegramChatId != 0 {
		go func() {
			log.Println(runTelegramLoop(config))
		}()
	}

	logEntryChain := make(chan types.RequestLogEntry, 100)
	webSocketlogEntryChain := make(chan types.WebSocketMessage, 100)

	logger, err := New(config.MongoUrl, config.AppName)
	if err != nil {
		log.Fatal(err)
	}
	logger.Setup()

	analyzerObj := analyzer.New(config)
	go logEntryHandler(logEntryChain, logger, analyzerObj)
	go webSocketLogEntryHandler(webSocketlogEntryChain, logger, analyzerObj)

	rpURL, err := url.Parse(config.ServiceUrl)
	if err != nil {
		log.Fatal(err)
	}
	s := &http.Server{
		Addr:           ":8080",
		Handler:        createProxy(rpURL, logEntryChain, webSocketlogEntryChain, config),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Print("Starting server on 8080")
	log.Fatal(s.ListenAndServe())
}
