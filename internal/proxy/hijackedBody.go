package proxy

import (
	"bytes"
	"io"
)

type HijackedBody struct {
	Body io.ReadCloser
	Data *bytes.Buffer
}

func NewHijackedBody(body io.ReadCloser) *HijackedBody {
	return &HijackedBody{
		Body: body,
		Data: bytes.NewBuffer(make([]byte, 0, 32*1024)),
	}
}

func (b *HijackedBody) Read(p []byte) (int, error) {
	n, err := b.Body.Read(p)
	if n == 0 {
		return n, err
	}
	b.Data.Write(p)
	return n, err
}

func (b *HijackedBody) Close() error {
	return b.Body.Close()
}
