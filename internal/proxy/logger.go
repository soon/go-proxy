package proxy

import (
	"proxy/internal/db"
	"proxy/internal/types"
)

type Logger struct {
	connection *db.Connection
}

func New(mongoUri string, appName string) (*Logger, error) {
	conn, err := db.New(mongoUri, appName)
	if err != nil {
		return nil, err
	}
	l := Logger{
		connection: conn,
	}
	return &l, nil
}

func (l *Logger) Setup() {
	l.connection.Setup()
}

func (l *Logger) LogEntry(entry types.RequestLogEntry) error {
	return l.connection.AddLogEntry(entry)
}

func (l *Logger) LogWsEntry(entry types.WebSocketMessage) error {
	return l.connection.AddWsLogEntry(entry)
}
