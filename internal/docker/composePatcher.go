package docker

import (
	"errors"
	"github.com/manifoldco/promptui"
	"gopkg.in/yaml.v3"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func newError(text string) error {
	return errors.New("composePatcher: " + text)
}

func getFirstMapKey(m map[string]interface{}) string {
	for k := range m {
		return k
	}
	panic("expected an non-empty map")
}

func getMapKeys(m map[string]interface{}) []string {
	keys := make([]string, len(m))

	i := 0
	for k := range m {
		keys[i] = k
		i++
	}
	return keys
}

func AddProxyToDockerCompose(composeFilePath string) error {
	f, err := os.Open(composeFilePath)
	if err != nil {
		return err
	}
	decoder := yaml.NewDecoder(f)
	content := make(map[string]interface{})
	err = decoder.Decode(content)
	ferr := f.Close()
	if err != nil {
		return err
	}
	if ferr != nil {
		return ferr
	}
	services := content["services"]
	if services == nil {
		return newError("'services' section not found")
	}
	servicesMap, ok := services.(map[string]interface{})
	if !ok {
		return newError("'services' is not a map")
	}

	servicesWithExposedPorts := make(map[string]interface{})
	for s := range servicesMap {
		ports := getServicePorts(servicesMap, s)
		if ports != nil {
			servicesWithExposedPorts[s] = ports
		}
	}

	if len(servicesWithExposedPorts) == 0 {
		return newError("unable to find a service with exposed ports")
	}
	serviceName := getFirstMapKey(servicesWithExposedPorts)
	if len(servicesWithExposedPorts) > 1 {
		serviceNameSelector := promptui.Select{
			Label: "There are " + strconv.Itoa(len(servicesWithExposedPorts)) + " services with exposed ports",
			Items: getMapKeys(servicesWithExposedPorts),
		}
		_, result, err := serviceNameSelector.Run()
		if err != nil {
			return err
		}
		serviceName = result
	}

	ports, ok := servicesWithExposedPorts[serviceName].([]interface{})
	if !ok {
		return newError("expected ports to be array (service '" + serviceName + "')")
	}
	if len(ports) != 1 {
		return newError("expected ports to have exactly one exposed port (service '" + serviceName + "')")
	}
	port, ok := ports[0].(string)
	if !ok {
		return newError("expected ports to be a string array (service '" + serviceName + "')")
	}
	if matchString, err := regexp.MatchString("^\\d+:\\d+$", port); !matchString || err != nil {
		return newError("expected port to be a string like 5000:5000 (service '" + serviceName + "')")
	}
	portParts := strings.Split(port, ":")
	hostPort := portParts[0]
	containerPort := portParts[1]

	service := servicesMap[serviceName].(map[string]interface{})
	delete(service, "ports")

	mongoService, mongoServiceName := createProxyMongoService(servicesMap)
	servicesMap[mongoServiceName] = mongoService

	redisService, redisServiceName := createProxyRedisService(servicesMap)
	servicesMap[redisServiceName] = redisService

	proxyService, proxyServiceName := createProxyService(servicesMap, ProxyServiceConfig{
		webService:   serviceName,
		webPort:      containerPort,
		hostPort:     hostPort,
		mongoService: mongoServiceName,
		redisService: redisServiceName,
	})
	servicesMap[proxyServiceName] = proxyService

	err = os.Rename(composeFilePath, composeFilePath+".original")
	if err != nil {
		return err
	}
	f, err = os.Create(composeFilePath)
	if err != nil {
		return err
	}
	encoder := yaml.NewEncoder(f)
	encoder.SetIndent(2)
	err = encoder.Encode(content)
	ferr = f.Close()
	if err != nil {
		return err
	}
	return ferr
}

func selectServiceName(servicesMap map[string]interface{}, name string) string {
	if _, contains := servicesMap[name]; !contains {
		return name
	}
	idx := 1

	for {
		_, contains := servicesMap[name+"-"+strconv.Itoa(idx)]
		if !contains {
			break
		}
		idx += 1
	}
	return name + "-" + strconv.Itoa(idx)
}

func createProxyMongoService(servicesMap map[string]interface{}) (map[string]interface{}, string) {
	name := selectServiceName(servicesMap, "proxy-mongo")
	return map[string]interface{}{
		"image": "mongo:4.2",
	}, name
}

func createProxyRedisService(servicesMap map[string]interface{}) (map[string]interface{}, string) {
	name := selectServiceName(servicesMap, "proxy-redis")
	return map[string]interface{}{
		"image": "redis:5",
	}, name
}

type ProxyServiceConfig struct {
	webService   string
	webPort      string
	hostPort     string
	mongoService string
	redisService string
}

func createProxyService(servicesMap map[string]interface{}, cfg ProxyServiceConfig) (map[string]interface{}, string) {
	name := selectServiceName(servicesMap, "proxy")
	return map[string]interface{}{
		"links": [...]string{
			cfg.webService,
			cfg.mongoService,
			cfg.redisService,
		},
		"depends_on": [...]string{
			cfg.webService,
			cfg.mongoService,
			cfg.redisService,
		},
		"environment": map[string]string{
			"PROXY_APP_NAME":  "web",
			"PROXY_URL":       cfg.webService + ":" + cfg.webPort,
			"PROXY_MONGO_URL": "mongodb://" + cfg.mongoService + ":27017",
			"PROXY_REDIS_URL": cfg.redisService + ":6379",
		},
		"build": map[string]string{
			"context":    "go-proxy",
			"dockerfile": "Dockerfile",
		},
		"ports": [...]string{
			cfg.hostPort + ":" + "8080",
		},
	}, name
}

func getServicePorts(servicesMap map[string]interface{}, serviceName string) interface{} {
	service, ok := servicesMap[serviceName].(map[string]interface{})
	if !ok {
		return nil
	}
	return service["ports"]
}
