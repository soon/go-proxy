# Useful commands

1. Run `./go-proxy/bin/linux/docker-util` in a folder with `docker-compose.yml` file to add proxy to docker-compose
2. Run `docker-compose up -d --build proxy` after changing plugin to compile and recreate proxy with changes
