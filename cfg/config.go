package cfg

import (
	"github.com/spf13/viper"
	"log"
	"strings"
)

type ApplicationConfig struct {
	EnablePprof                      bool
	ServiceUrl                       string
	MongoUrl                         string
	RedisUrl                         string
	LogResponseBodyWhenContentTypeIs []string
	AppName                          string
	UseIpFromXForwardedFor           bool
	UseColors                        bool
	AdminIpRegex                     string
	FlagRegex                        string
	TelegramToken                    string
	TelegramChatId                   int64
}

const EnvPrefix = "proxy"

func mergeWithEnvPrefix(in string) string {
	return strings.ToUpper(EnvPrefix + "_" + in)
}

type Viper viper.Viper

func getRequiredString(v *viper.Viper, key string) string {
	value := v.GetString(key)
	if value == "" {
		log.Fatal(mergeWithEnvPrefix(key) + " is required")
	}
	return value
}

func getServiceUrl(v *viper.Viper) string {
	serviceUrl := getRequiredString(v, "url")
	if !strings.Contains(serviceUrl, "://") {
		serviceUrl = "http://" + serviceUrl
	}
	return serviceUrl
}

func GetApplicationConfig() ApplicationConfig {
	v := viper.New()
	v.SetEnvPrefix(EnvPrefix)
	v.AutomaticEnv()

	v.SetDefault("url", "http://localhost:8000")
	v.SetDefault("mongo_url", "mongodb://localhost:27017")
	v.SetDefault("redis_url", "localhost:6379")
	v.SetDefault("use_ip_from_x_forwarded_for", false)
	v.SetDefault("colors", true)
	v.SetDefault("flag_regex", "[A-Z0-9]{31}=")
	v.SetDefault("admin_ip_regex", "10.80.1.1")
	return ApplicationConfig{
		EnablePprof:                      v.GetBool("enable_pprof"),
		ServiceUrl:                       getServiceUrl(v),
		MongoUrl:                         getRequiredString(v, "mongo_url"),
		RedisUrl:                         getRequiredString(v, "redis_url"),
		LogResponseBodyWhenContentTypeIs: v.GetStringSlice("log_response_body_when_content_type_is"),
		AppName:                          getRequiredString(v, "app_name"),
		UseIpFromXForwardedFor:           v.GetBool("use_ip_from_x_forwarded_for"),
		UseColors:                        v.GetBool("colors"),
		FlagRegex:                        getRequiredString(v, "flag_regex"),
		AdminIpRegex:                     getRequiredString(v, "admin_ip_regex"),
		TelegramToken:                    v.GetString("telegram_token"),
		TelegramChatId:                   v.GetInt64("telegram_chat_id"),
	}
}
