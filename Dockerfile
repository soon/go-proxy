FROM golang:1.13

ENV GO111MODULE=on

WORKDIR /go/src/app

COPY go.mod ./
RUN go mod download

COPY . .

RUN cd /go/src/app/cmd/cli && go build -o cli .

WORKDIR /go/src/app/cmd/proxy
RUN go build -o proxy .

EXPOSE 8080
EXPOSE 6060

ENV PATH="/go/src/app/cmd/cli:/go/src/app/cmd/proxy:${PATH}"

CMD ["proxy"]
