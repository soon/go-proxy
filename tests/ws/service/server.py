import argparse
import json

from aiohttp import web, WSMsgType

routes = web.RouteTableDef()

FLAG = None


@routes.get('/ws')
async def flags_ws(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        global FLAG
        if msg.type == WSMsgType.TEXT:
            payload = json.loads(msg.data)
            if payload['type'] == 'put-flag':
                global FLAG
                FLAG = payload['flag']
            elif payload['type'] == 'get-flag':
                await ws.send_str(FLAG)

    return ws


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    app = web.Application()
    app.add_routes(routes)
    web.run_app(app)
