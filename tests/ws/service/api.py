import argparse
import asyncio
import json
import os

import aiohttp

SERVICE_URL = None

timeout = aiohttp.ClientTimeout(total=1)


async def put_flag(flag):
    async with aiohttp.ClientSession(timeout=timeout) as session:
        async with session.ws_connect(f'{SERVICE_URL}/ws') as ws:
            await ws.send_str(json.dumps({
                'type': 'put-flag',
                'flag': flag,
            }))


async def get_flag():
    async with aiohttp.ClientSession(timeout=timeout) as session:
        headers = {'X-Forwarded-For': '123.45.67.89'}
        async with session.ws_connect(f'{SERVICE_URL}/ws', headers=headers) as ws:
            await ws.send_str(json.dumps({
                'type': 'get-flag',
            }))
            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.TEXT:
                    return msg.data
                raise ValueError('Unexpected msg')
            raise ValueError('Expected msg with flag')


async def print_flag():
    print(await get_flag())


if __name__ == '__main__':
    try:
        SERVICE_URL = os.environ['SERVICE_URL']
    except KeyError:
        print('SERVICE_URL env variable is required')
        exit(1)
    if '://' not in SERVICE_URL:
        SERVICE_URL = f'http://{SERVICE_URL}'
    SERVICE_URL = SERVICE_URL.rstrip('/')

    parser = argparse.ArgumentParser()

    parser.add_argument("--type", type=str, required=True)
    parser.add_argument("--flag", type=str)
    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    if args.type == 'get-flag':
        fn = print_flag()
    elif args.type == 'put-flag':
        if not args.flag:
            raise ValueError('Flag is required')
        fn = put_flag(args.flag)
    else:
        raise ValueError(f'Unknown type: {args.type}')
    loop.run_until_complete(fn)
