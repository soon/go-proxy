set -e

docker-compose down
docker-compose up --build -d
docker-compose run api --type put-flag --flag 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX=
docker-compose run api --type get-flag | grep 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX=
echo "fs" | docker-compose exec -T proxy cli | grep 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX=
REQUEST_ID=`echo "f 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX=" | docker-compose exec -T proxy cli | grep Stolen | awk '{split($0,a,": "); print a[2]}'`
echo "r ${REQUEST_ID}" | docker-compose exec -T proxy cli | grep '{"type": "get-flag"}'
echo "r ${REQUEST_ID}" | docker-compose exec -T proxy cli | grep "69G5U0X53HXPUZQ8POVTTAQS2JGF6LX="
# docker-compose down
