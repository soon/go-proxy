package proxyPlugin

import (
	"proxy/internal/types"
	"strings"
)

func ShouldInterruptRequest(req types.RequestLogEntry) bool {
	return strings.Contains(req.Url, "attack")
}
