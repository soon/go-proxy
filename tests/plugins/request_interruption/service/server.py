from flask import Flask, request

app = Flask(__name__)

FLAGS = []


@app.route('/flags', methods=['POST'])
def add_flag():
    if request.method == 'POST':
        FLAGS.append(request.form['flag'])
    return "OK"


@app.route('/flags/<idx>')
def get_flag(idx):
    return FLAGS[int(idx)]
