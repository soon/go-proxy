import argparse
import json
import os
import requests

SERVICE_URL = None


def put_flag(flag):
    requests.post(f'{SERVICE_URL}/flags', data={
        'flag': flag
    })


def steal_flag():
    return requests.get(f'{SERVICE_URL}/flags/0?attack').text


def get_flag():
    return requests.get(f'{SERVICE_URL}/flags/0').text


if __name__ == '__main__':
    try:
        SERVICE_URL = os.environ['SERVICE_URL']
    except KeyError:
        print('SERVICE_URL env variable is required')
        exit(1)
    if '://' not in SERVICE_URL:
        SERVICE_URL = f'http://{SERVICE_URL}'
    SERVICE_URL = SERVICE_URL.rstrip('/')

    parser = argparse.ArgumentParser()

    parser.add_argument("--type", type=str, required=True)
    parser.add_argument("--flag", type=str)
    args = parser.parse_args()

    if args.type == 'get-flag':
        fn = lambda: print(get_flag())
    elif args.type == 'steal-flag':
        fn = lambda: print(steal_flag())
    elif args.type == 'put-flag':
        if not args.flag:
            raise ValueError('Flag is required')
        fn = lambda: put_flag(args.flag)
    else:
        raise ValueError(f'Unknown type: {args.type}')
    fn()
