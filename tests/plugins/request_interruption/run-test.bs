set -e

cp ../../../Dockerfile ./proxy/Dockerfile
sed -i '' '/^COPY \. \./a\
COPY ./tests/plugins/request_interruption/proxy/proxyPlugin.go plugin/proxy/proxyPlugin.go
' ./proxy/Dockerfile

docker-compose down
docker-compose up --build -d
docker-compose run api --type put-flag --flag 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX= || (echo "fail to put flag" && false)
docker-compose run api --type get-flag | grep 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX= || (echo "fail to get flag" && false)
docker-compose run api --type steal-flag | grep -v 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX= || (echo "fail to steal flag" && false)
echo "fs" | docker-compose exec -T proxy cli | grep 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX= || (echo "fail to verify stolen flag" && false)
REQUEST_ID=`echo "f 69G5U0X53HXPUZQ8POVTTAQS2JGF6LX=" | docker-compose exec -T proxy cli | grep Interrupted | awk '{split($0,a,":"); print a[2]}'`
echo "r ${REQUEST_ID}" | docker-compose exec -T proxy cli | grep '/flags/0?attack' || (echo "fail to verify request url" && false)
echo "r ${REQUEST_ID}" | docker-compose exec -T proxy cli | grep "69G5U0X53HXPUZQ8POVTTAQS2JGF6LX=" || (echo "fail to verify request response" && false)
echo "r ${REQUEST_ID}" | docker-compose exec -T proxy cli | grep INTERRUPTED || (echo "fail to verify request is interrupted" && false)
# docker-compose down
