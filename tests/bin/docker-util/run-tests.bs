set -e

CWD=`pwd`
PROJECT_ROOT=`realpath ${CWD}/../../..`

if [[ "$OSTYPE" == "darwin"* ]]; then
    BIN_PATH=${PROJECT_ROOT}/bin/darwin
else
    BIN_PATH=${PROJECT_ROOT}/bin/linux
fi
DOCKER_UTIL_BIN=${BIN_PATH}/docker-util

for dir in ./test-data/*/
do
    dir=${dir%*/}
    printf "Running ${dir##*/}... " -n
    ${DOCKER_UTIL_BIN} ${dir}/docker-compose.yml
    diff ${dir}/docker-compose.yml ${dir}/docker-compose.expected.yml
    mv ${dir}/docker-compose.yml.original ${dir}/docker-compose.yml
    echo ✓
done
