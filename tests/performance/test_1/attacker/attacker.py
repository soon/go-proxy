import argparse
import asyncio
import os
from datetime import datetime, timedelta
from ipaddress import IPv4Address
from random import getrandbits

import aiohttp

SERVICE_URL = None

timeout = aiohttp.ClientTimeout(total=10)


class TypeDelta:
    def __call__(self, value):
        if value.endswith('s'):
            return timedelta(seconds=int(value[:-1]))
        if value.endswith('m'):
            return timedelta(minutes=int(value[:-1]))
        raise ValueError(f'Unknown value: {value}')


def generate_random_ip():
    bits = getrandbits(32)
    addr = IPv4Address(bits)
    return str(addr)


async def attack(attacker_ip: str):
    idx = 0
    total_requests = 0
    async with aiohttp.ClientSession(timeout=timeout) as session:
        while True:
            async with session.get(f'{SERVICE_URL}/flags/{idx}') as response:
                total_requests += 1
                if response.status != 200:
                    return total_requests
            idx += 1


async def run(attackers_count: int):
    res = await asyncio.gather(*(
        attack(generate_random_ip())
        for _ in range(attackers_count)
    ))
    return sum(res)


def main():
    global SERVICE_URL
    try:
        SERVICE_URL = os.environ['SERVICE_URL']
    except KeyError:
        print('SERVICE_URL env variable is required')
        return 1
    if '://' not in SERVICE_URL:
        SERVICE_URL = f'http://{SERVICE_URL}'
    SERVICE_URL = SERVICE_URL.rstrip('/')

    parser = argparse.ArgumentParser()

    parser.add_argument("--duration", type=TypeDelta(), default='1m')
    args = parser.parse_args()

    start = datetime.now()
    total_requests = 0
    loop = asyncio.get_event_loop()
    while datetime.now() < start + args.duration:
        prev_iteration_time = datetime.now()
        current_requests = loop.run_until_complete(run(100))
        now = datetime.now()

        total_requests += current_requests
        total_delta = now - start
        total_rps = int(total_requests / (total_delta.total_seconds() or 1))

        iteration_delta = now - prev_iteration_time
        iteration_rps = int(current_requests / (iteration_delta.total_seconds() or 1))
        print(f'{total_delta}: {total_requests} requests ({total_rps} total rps, {iteration_rps} iteration rps)')
    loop.close()


if __name__ == '__main__':
    exit(main())
