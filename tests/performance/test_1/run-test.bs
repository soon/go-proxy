set -e

docker-compose down
docker-compose up --build -d
echo "Without proxy"
docker-compose run attacker attacker.py "$@"
docker-compose down
SERVICE_URL=proxy:8080 docker-compose up -d
echo ----------
echo With proxy
SERVICE_URL=proxy:8080 docker-compose run attacker attacker.py "$@"
echo "fs" | docker-compose exec -T proxy cli | grep "Showing 10 of"
# docker-compose down
