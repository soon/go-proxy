import os
import random
import string
import time

import requests

SERVICE_URL = None


def put_flag():
    flag = ''.join(random.choices(string.ascii_uppercase + string.digits, k=31)) + '='
    requests.post(f'{SERVICE_URL}/flags', data={
        'flag': flag
    })


if __name__ == '__main__':
    try:
        SERVICE_URL = os.environ['SERVICE_URL']
    except KeyError:
        print('SERVICE_URL env variable is required')
        exit(1)
    if '://' not in SERVICE_URL:
        SERVICE_URL = f'http://{SERVICE_URL}'
    SERVICE_URL = SERVICE_URL.rstrip('/')

    time.sleep(5)
    while True:
        for i in range(10):
            put_flag()
        time.sleep(20)
