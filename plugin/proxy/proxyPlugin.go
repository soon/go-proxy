package proxyPlugin

import (
	"proxy/internal/types"
	"strings"
)

func UrlContainsIgnoreCase(req types.RequestLogEntry, substr string) bool {
	return strings.Contains(strings.ToLower(req.Url), strings.ToLower(substr))
}

func getUserAgent(req types.RequestLogEntry) string {
	return req.RequestHeaders.Get("User-Agent")
}

func UserAgentHasPrefixIgnoreCase(req types.RequestLogEntry, prefix string) bool {
	return strings.HasPrefix(strings.ToLower(getUserAgent(req)), strings.ToLower(prefix))
}

func NoUserAgent(req types.RequestLogEntry) bool {
	return getUserAgent(req) == ""
}

func IsPythonRequests(req types.RequestLogEntry) bool {
	return UserAgentHasPrefixIgnoreCase(req, "python-requests/")
}

func getContentType(req types.RequestLogEntry) string {
	return req.RequestHeaders.Get("Content-Type")
}

func IsFormUrlencoded(req types.RequestLogEntry) bool {
	return getContentType(req) == "application/x-www-form-urlencoded"
}

// Use "docker-compose up -d --build proxy" to apply changes
func ShouldInterruptRequest(req types.RequestLogEntry) bool {
	return false
}
