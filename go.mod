module proxy

go 1.13

require (
	github.com/abiosoft/ishell v2.0.0+incompatible // indirect
	github.com/abiosoft/readline v0.0.0-20180607040430-155bce2042db
	github.com/fatih/color v1.9.0
	github.com/flynn-archive/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/go-redis/redis/v7 v7.0.0-beta.6
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/google/uuid v1.1.1
	github.com/manifoldco/promptui v0.7.0
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.3.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	go.mongodb.org/mongo-driver v1.3.0
	gopkg.in/abiosoft/ishell.v2 v2.0.0
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
